import React, { useState, useEffect } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Frase from "./components/Frase";
import logo from "./img/logosimpsons.png";
import Spinner from "./components/Spinner";

function App() {
  const [fraseSimpsons, setFraseSimpsons] = useState({});
  const [loader, setLoader] = useState(false);

  const consultarAPI = async () => {
    setLoader(true);

    const api = await fetch("https://thesimpsonsquoteapi.glitch.me/quotes", {
      mode: "cors",
    });
    const respuesta = await api.json();
    console.log(api);
    console.log(respuesta[0]);
    setTimeout(()=>{
      setFraseSimpsons(respuesta[0]);
    setLoader(false);
    }, 1500);

    
  };

  useEffect(() => {
    consultarAPI();
  }, []);

  const cargarComponente = loader ? (
    <Spinner></Spinner>
  ) : (
    <Frase fraseSimpsons={fraseSimpsons}></Frase>
  );

  return (
    <section className="container my-4">
      <article className="d-flex flex-column align-items-center">
        <img src={logo} alt="logo Simpsons" className="w-75" />
        <button
          className="btn btn-warning w-25 my-4 shadow"
          onClick={() => consultarAPI()}
        >
          Obtener Frase
        </button>
      </article>
      {cargarComponente}
    </section>
  );
}

export default App;
