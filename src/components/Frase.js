import React from "react";

const Frase = (props) => {
  return (
    <div>
      <div className="card mb-3">
        <div className="row no-gutters">
          <div className="col-md-3">
            <img
              src={props.fraseSimpsons.image}
              className="card-img w-50 m-3"
              alt="Imagen personaje Simpsons"
            />
          </div>
          <div className="col-md-9">
            <div className="card-body">
              <h5 className="card-title text-center">
                {props.fraseSimpsons.character}
              </h5>
              <p className="card-text text-center">{props.fraseSimpsons.quote} </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Frase;
